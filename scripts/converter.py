#!/usr/bin/env python
# license removed for brevity
import rospy
from geographic_msgs.msg import GeoPointStamped
from geographic_msgs.msg import GeoPoint
from std_msgs.msg import String
from novatel_msgs.msg import INSPVAX 

def callback(data):
    rospy.loginfo(rospy.get_caller_id() + "I heard %f", data.latitude)
    converted = GeoPointStamped()
    payload = GeoPoint()
    payload.latitude = data.latitude
    payload.longitude = data.longitude
    #payload.altitude = data.altitude
    converted.position = payload
    rospy.loginfo(payload.latitude)
    pub.publish(converted)


def converter():
    rospy.Subscriber("span/novatel_data/inspvax", INSPVAX, callback)
    rospy.init_node('converter', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    rospy.spin()

if __name__ == '__main__':
    pub = rospy.Publisher('gps/geopoint', GeoPointStamped, queue_size=10)
    try:
        converter()
    except rospy.ROSInterruptException:
        pass

